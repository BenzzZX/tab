// Fill out your copyright notice in the Description page of Project Settings.

#include "TABGameMode.h"
#include "TABPlayerCharacter.h"
#include "TABGameSession.h"
#include "TABPlayerState.h"
#include "TABPlayerController.h"
#include "TABGameState.h"
#include "TABGameInstance.h"
#include "TABPlayerState.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "TAB.h"


ATABGameMode::ATABGameMode()
{
	DefaultPawnClass = ATABPlayerCharacter::StaticClass();
	GameStateClass = ATABGameState::StaticClass();
	PlayerControllerClass = ATABPlayerController::StaticClass();
	PlayerStateClass = ATABPlayerState::StaticClass();

	TeamRemain = 0;
}


void ATABGameMode::BeginPlay()
{
}

void ATABGameMode::RequestFinishAndExitToRoom()
{

	FinishMatch();

	//UTABGameInstance* const GameInstance = Cast<UTABGameInstance>(GetGameInstance());

	ATABPlayerController* LocalPrimaryController = nullptr;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		ATABPlayerController* Controller = Cast<ATABPlayerController>(*Iterator);

		if (Controller == NULL)
		{
			continue;
		}

		if (!Controller->IsLocalController())
		{
			const FString RemoteReturnReason = NSLOCTEXT("NetworkErrors", "HostHasLeft", "Host has left the game.").ToString();
			Controller->ClientReturnToMainMenu(RemoteReturnReason);
		}
		else
		{
			LocalPrimaryController = Controller;
		}
	}

	// GameInstance should be calling this from an EndState.  So call the PC function that performs cleanup, not the one that sets GI state.
	if (LocalPrimaryController != NULL)
	{
		LocalPrimaryController->HandleReturnToRoom();
	}
}

void ATABGameMode::ChangeTeamLife(int32 TeamID, int32 Delta)
{
	ATABGameState* TABGameState = GetGameState<ATABGameState>();
	//UE_LOG(LogTABGame, Log, TEXT("Try to change team %d life , delta = "), TeamID, Delta);
	if (IsValid(TABGameState))
	{
		if (TeamID < TABGameState->NumTeams)
		{
			//GameMode没有GetMaxLife这个函数，所以这里我注释掉了， 就得到最大生命值
			TABGameState->TeamLife[TeamID] = FMath::Clamp(TABGameState->TeamLife[TeamID] + Delta, 0, MaxLife);
			UE_LOG(LogTABGame, Log, TEXT("Change team %d life to %d"), TeamID, TABGameState->TeamLife[TeamID]);
			TABGameState->OnTeamLifeChange.Broadcast(TABGameState->TeamLife);
			if (TABGameState->TeamLife[TeamID] == 0)
			{
				TeamLose(TeamID);
			}
			else if (TABGameState->TeamLife[TeamID] == MaxLife)
			{
				TeamWin(TeamID);
			}
		}
	}
}

void ATABGameMode::FinishMatch()
{
	//ATABGameState* const MyGameState = Cast<ATABGameState>(GameState);
	if (IsMatchInProgress())
	{
		EndMatch();

		// lock all pawns
		// pawns are not marked as keep for seamless travel, so we will create new pawns on the next match rather than
		// turning these back on.

		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
		{
			(*It)->TurnOff();
		}

		// set up to restart the match
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ATABGameMode::HandleShowWinnerAndRestart, false);
	}
}

void ATABGameMode::TeamLose(int32 TeamID)
{
	ATABGameState* TABGameState = GetGameState<ATABGameState>();
	// notify players
	TeamRemain -= 1;
	if (TeamRemain == 1)
	{
		for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
		{
			ATABPlayerState* PlayerState = Cast<ATABPlayerState>((*It)->PlayerState);
			if (TABGameState->TeamLife[PlayerState->GetTeamID()] > 0)
				(*It)->GameHasEnded(NULL, true);
		}
		FinishMatch();
	}
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ATABPlayerState* PlayerState = Cast<ATABPlayerState>((*It)->PlayerState);
		if (PlayerState->GetTeamID() == TeamID)
		{
			(*It)->GameHasEnded(NULL, false);
			PlayerState->bLosed = true;
		}
	}
}

void ATABGameMode::TeamWin(int32 TeamID)
{
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ATABPlayerState* PlayerState = Cast<ATABPlayerState>((*It)->PlayerState);
		if (PlayerState->GetTeamID() == TeamID)
			(*It)->GameHasEnded(NULL, true);
		else if (!PlayerState->bLosed)
		{
			(*It)->GameHasEnded(NULL, false);
			PlayerState->bLosed = true;
		}
	}
	FinishMatch();
}

void ATABGameMode::HandleShowWinnerAndRestart_Implementation()
{
	GetWorldTimerManager().ClearTimer(TimerHandle);
}

/** Returns game session class to use */
TSubclassOf<AGameSession> ATABGameMode::GetGameSessionClass() const
{
	return ATABGameSession::StaticClass();
}

void ATABGameMode::CheckReadyPlayerNum()
{
	ATABGameState* GameState = GetGameState<ATABGameState>();

	OnPlayerReady.Broadcast(GameState->ReadyPlayerNum, GetNumPlayers());
	if (GameState->ReadyPlayerNum == GetNumPlayers())
	{
		GetWorldTimerManager().SetTimer(GameStartTimer, this, &ATABGameMode::CheckTime, 1.f, true);
		GameState->RemainTime = 10;
		
	}
	else if(GameState->RemainTime > 6)
	{
		GetWorldTimerManager().ClearTimer(GameStartTimer);
		GameState->RemainTime = 10;
	}
}

void ATABGameMode::CheckTime()
{
	ATABGameState* GameState = GetGameState<ATABGameState>();
	GameState->RemainTime--;
	OnCountDown.Broadcast(GameState->RemainTime);
	if (GameState->RemainTime == 0)
	{
		GetWorldTimerManager().ClearTimer(GameStartTimer);
		GameState->TeamLife.SetNumZeroed(GameState->NumTeams);
		for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
		{
			ATABPlayerState* PlayerState = Cast<ATABPlayerState>((*It)->PlayerState);
			int& Life = GameState->TeamLife[PlayerState->GetTeamID()];
			if (Life == 0) 
				TeamRemain++;
			Life = InitLife;
			GameState->OnTeamLifeChange.Broadcast(GameState->TeamLife);
		}
	}
		
}

void ATABGameMode::AddReadyPlayerNum()
{
	ATABGameState* GameState = GetGameState<ATABGameState>();
	GameState->ReadyPlayerNum++;
	CheckReadyPlayerNum();
}
void ATABGameMode::SubReadyPlayerNum()
{
	ATABGameState* GameState = GetGameState<ATABGameState>();
	GameState->ReadyPlayerNum--;
	CheckReadyPlayerNum();
}