// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TABFood.h"
#include "GameFramework/Actor.h"
#include "TABRestaurant.generated.h"



UCLASS()
class TAB_API ATABRestaurant : public AActor
{
	GENERATED_BODY()
	
public:
	ATABRestaurant();

	/**
	* 生产食物

	* @param FTABFoodInfo 食物的信息
	* @param float 客户忍耐时间
	*/
	UFUNCTION(Category = "Order", BlueprintCallable)
	void ProductFood(int32 TeamID);
	/**
	* 传递食物
	*
	* @param ATABPlayerCharacter 传递给谁
	*/
	void GiveFood(class ATABPlayerCharacter* PlayerPawn);
	
	/**
	* 与玩家进行交互
	*
	* @param ATABPlayerCharacter 交互对象
	*/
	UFUNCTION()
	void InteractWith(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);
	
	UFUNCTION()
	void ReceiveFocus(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Food")
	FTABFoodInfo FoodInfo;

	UPROPERTY(BlueprintReadWrite, Category = "Order")
	class ATABFood* FoodReady;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interact")
	class UTABInteractiveComponent* InteractiveComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Food")
	TSubclassOf<ATABFood> FoodClass;

	UFUNCTION(BlueprintImplementableEvent)
	void OnProductFood();

	class ATABOrderGenerator* Company;
};
