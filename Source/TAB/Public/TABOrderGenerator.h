// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TABOrderGenerator.generated.h"

UCLASS()
class TAB_API ATABOrderGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATABOrderGenerator();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Interval = 20;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float OrderLimitTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float OrderPerfectTime;
	
	TArray<AActor*> AvailableHouseholds;
	TArray<AActor*> AvailableRestaurants;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void RegisterHousehold(class ATABHousehold* Household);

	UFUNCTION()
	void RegisterRestaurant(class ATABRestaurant* Restaurant);

private:
	void OnGenerate();

	bool GetATeam(int32&);
	
	FTimerHandle TimerHandle;
};
