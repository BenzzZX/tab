// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "TABGameState.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTeamLifeChangeSignature, const TArray<int32>&, TeamLifes);
/**
 * 
 */
UCLASS()
class TAB_API ATABGameState : public AGameState
{
	GENERATED_BODY()
	
public:

	ATABGameState();

	/** number of teams in current game (doesn't deprecate when no players are left in a team) */
	UPROPERTY(Transient, Replicated)
	int32 NumTeams = 4;

	/** Life of each team */
	UPROPERTY(Transient, ReplicatedUsing=OnRep_TeamLife)
	TArray<int32> TeamLife;

	void RequestFinishAndExitToRoom();

	UPROPERTY(Transient, Replicated)
	int32 RemainTime;

	UPROPERTY(Transient, Replicated)
	int32 ReadyPlayerNum;

	UPROPERTY(BlueprintAssignable)
	FOnTeamLifeChangeSignature OnTeamLifeChange;

protected:
	UFUNCTION()
	void OnRep_TeamLife();
};
