// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TABItem.h"
#include "TABFood.generated.h"

UENUM(BlueprintType)
enum class ETABFoodType : uint8
{
	Hamburger,
	Chip,
	Coffee,
};


USTRUCT(BlueprintType)
struct FTABFoodInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	ETABFoodType Type;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UStaticMesh* Mesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float ShelfLife;
};


UCLASS()
class TAB_API ATABFood : public ATABItem
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATABFood();

	//NanGua
	void BecomeOutdue();
	virtual void OnUse(class ATABPlayerCharacter*);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnUse_Implementation(class ATABPlayerCharacter* PlayerCharacter) override;

	virtual bool CanPickBy_Implementation(class ATABPlayerCharacter* PlayerCharacter) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated)
	FTABFoodInfo Info;

	UPROPERTY()
	float TimeToBeOverdue;

	UPROPERTY(BlueprintReadOnly, Replicated)
	int32 TeamID;

	UPROPERTY(BlueprintReadOnly, Replicated)
	uint8 bIsOverdue : 1;


private:
	FTimerHandle TimeToBeOverdueTimerHandle;
};
