// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "TABUserSettings.generated.h"

/**
 * 
 */
UCLASS()
class TAB_API UTABUserSettings : public UGameUserSettings
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY()
	int32 GraphicQuality;

	UPROPERTY()
	uint8 bIsLanMatch : 1;

	virtual void SetToDefaults() override;
	virtual void ApplySettings(bool bCheckForCommandLineOverrides) override;
};
