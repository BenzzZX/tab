// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineSessionClient.h"
#include "TABOnlineSessionClient.generated.h"

/**
 * 
 */
UCLASS()
class TAB_API UTABOnlineSessionClient : public UOnlineSessionClient
{
	GENERATED_BODY()
	
public:

	UTABOnlineSessionClient();

	virtual void OnSessionUserInviteAccepted(
		const bool							bWasSuccess,
		const int32							ControllerId,
		TSharedPtr< const FUniqueNetId >	UserId,
		const FOnlineSessionSearchResult &	InviteResult
	) override;

	virtual void OnPlayTogetherEventReceived(int32 UserIndex, TArray<TSharedPtr<const FUniqueNetId>> UserIdList) override;
	
	
};
