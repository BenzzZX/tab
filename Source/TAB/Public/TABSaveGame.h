// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "TABSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class TAB_API UTABSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UTABSaveGame();

	UPROPERTY()
	FString SlotName;

	UPROPERTY()
	int32 WinsNum;

	UPROPERTY()
	int32 LossesNum;

	UPROPERTY()
	int32 FinishNum;

	UPROPERTY()
	int32 PerfectNum;

	UPROPERTY()
	int32 SaveNum;

	UPROPERTY()
	uint8 bIsDirty : 1;
	
	void SetToDefaults();
	void SavePersistent();
	void AddMatchResult(int32 Finish, int32 Perfect, int32 Save, bool bIsWinner);
	void SaveIfDirty();
	static UTABSaveGame* LoadPersistent(FString SlotName);
};
