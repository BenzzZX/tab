// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TABCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class TAB_API UTABCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
	UTABCharacterMovementComponent(const FObjectInitializer& ObjectInitializer);

	friend class FSavedMove_TABCharacter;

	virtual void UpdateFromCompressedFlags(uint8 Flags) override;

	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;

	float ForwardRatio;
	float UpRatio;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jet")
	float FuelLeft;

	UPROPERTY(BlueprintReadWrite)
	uint8 bIsJetting : 1;
	
	virtual float GetMaxSpeed() const override;
protected:
	
	virtual void PerformMovement(float DeltaTime) override;

	virtual void OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity) override;

	virtual void HandleImpact(const FHitResult& Hit, float TimeSlice/* =0.f */, const FVector& MoveDelta /* = FVector::ZeroVector */) override;

	UPROPERTY(EditAnywhere, Category = "Jet")
	float JetStrength;

	UPROPERTY(EditAnywhere, Category = "Jet")
	float JetUpRatio;

	UPROPERTY(EditAnywhere, Category = "Direction")
	float TurnRate;

	UPROPERTY(EditAnywhere, Category = "Jet")
	float JetSpeedMultiplier;

	UPROPERTY(EditAnywhere, Category = "Jet")
	float FuelBurnRate;
private:

	FVector Facing;
};

class FSavedMove_TABCharacter : public FSavedMove_Character
{
	typedef FSavedMove_Character Super;

public:
	
	virtual void Clear() override;

	virtual uint8 GetCompressedFlags() const override;

	virtual bool CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* InPawn, float MaxDelta) const override;

	virtual void SetMoveFor(ACharacter* C, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData) override;

	virtual void PrepMoveFor(ACharacter* C) override;

private:
	float SavedFuelLeft;
	FVector SavedFacing;
	uint8 bSavedIsJetting : 1;
};


class FNetworkPredictionData_Client_TABCharacter : public FNetworkPredictionData_Client_Character
{
	typedef FNetworkPredictionData_Client_Character Super;
public:
	FNetworkPredictionData_Client_TABCharacter(const UCharacterMovementComponent& ClientMovement);

	virtual FSavedMovePtr AllocateNewMove() override;
};