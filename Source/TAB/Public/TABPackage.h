// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TABPackage.generated.h"


UCLASS()
class TAB_API ATABPackage : public AActor
{
	GENERATED_BODY()
public:
	ATABPackage();

	UPROPERTY(BlueprintReadOnly, meta = (ExposeOnSpawn), Replicated)
	class ATABItem* ItemIn;

	virtual void BeginPlay() override;
private:

	UPROPERTY(EditDefaultsOnly, Category = "Vanish")
	float TimeToVanish;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTABInteractiveComponent* InteractiveComponent;

	UFUNCTION()
	void OnReceiveFocus(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);

	UFUNCTION()
	void InteractWith(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);

	UFUNCTION()
	void Vanish();

	FTimerHandle TimerHandle;
};
