// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "TABPlayerCharacter.generated.h"



class UTABInteractiveComponent;
class ATABPlayerCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FChangeFocusingInteractiveSignature);

UCLASS()
class TAB_API ATABPlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATABPlayerCharacter(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(Category = "Interactive", BlueprintAssignable)
	FChangeFocusingInteractiveSignature OnChangeStatus;

	UFUNCTION(BlueprintCallable, Category = "Interactive")
	void TakeItem(class ATABItem* Item);

	UFUNCTION(BlueprintCallable, Category = "Interactive")
	class ATABFood* GiveFood();

	UPROPERTY(BlueprintReadOnly, Category = "Item", ReplicatedUsing = OnRep_Statu)
	class ATABFood* FoodCarrying;

	UPROPERTY(BlueprintReadOnly, Category = "Item", ReplicatedUsing = OnRep_Statu)
	TArray<class ATABItem*> ItemCarrying;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	int32 MaxItemNum;

	UPROPERTY(BlueprintReadOnly, Category = "Interactive", ReplicatedUsing = OnRep_Statu)
	UTABInteractiveComponent* FocusingInteractive;

	UFUNCTION(BlueprintCallable, Unreliable, Server, WithValidation)
	void ServerDrop();

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateTeamColor(int32 TeamID);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void DoDeathAnimation();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PlayThugLife();

	UFUNCTION(BlueprintPure)
	bool GetIsJetting();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class ATABPackage> FoodPackageClass;

	UFUNCTION(BlueprintCallable)
	void UpdateFocusing();

	UFUNCTION()
	void InteractWith(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);

	UFUNCTION()
	void ReceiveFocus(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);
protected:
	friend class UTABInteractiveComponent;
	friend class UTABCharacterMovementComponent;
	void NotifyEnterInteractive(UTABInteractiveComponent* InteractiveComponet);
	void NotifyLeaveInteractive(UTABInteractiveComponent* InteractiveComponet);

	UFUNCTION(BlueprintImplementableEvent)
	void OnBeginJetting();

	UFUNCTION(BlueprintImplementableEvent)
	void OnEndJetting();

	UFUNCTION(BlueprintImplementableEvent)
	void OnTakeItem(int32 Index);

	UFUNCTION(BlueprintImplementableEvent)
	void OnUseItem(int32 Index);

	UFUNCTION(BlueprintImplementableEvent)
	void OnTakeFood();

	UFUNCTION(BlueprintImplementableEvent)
	void OnGiveFood();


	UPROPERTY(Category = "Jet", VisibleAnywhere, BlueprintReadOnly)
	UParticleSystemComponent* Thruster_Forward;

	UPROPERTY(Category = "Jet", VisibleAnywhere, BlueprintReadOnly)
	UParticleSystemComponent* Thruster_Up;
private:
	bool bWantsToJet;

	UPROPERTY(Replicated)
	bool bIsJetting;

	UPROPERTY(Category = "Interactive", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true")) 
	UTABInteractiveComponent* InteractiveComponent;

	class UTABCharacterMovementComponent* CharMov;

	UPROPERTY()
	TArray<UTABInteractiveComponent*> PendingInteractives;

	UFUNCTION()
	void MoveUp(float delta);

	UFUNCTION()
	void MoveRight(float delta);

	UFUNCTION()
	void OnRep_Statu();

	UFUNCTION(Unreliable, Server, WithValidation)
	void ServerUse();

	UFUNCTION(Unreliable, Server, WithValidation)
	void ServerInteract();

	void InteractAction();

	void JetAction();

	void StopJetting();

	void UseAction();

	UFUNCTION()
	void Recovery();

	FTimerHandle Stunned;
};