// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TABPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TAB_API ATABPlayerState : public APlayerState
{
	GENERATED_BODY()
protected:

	void UpdateTeamColor();
	UPROPERTY(Transient, Replicated)
	int32 TeamID;

public:

	UPROPERTY(BlueprintReadWrite)
	float CarryTime; //time of carrying food

	UPROPERTY(BlueprintReadWrite)
	float GrabTime; //time of carrying others' food

	UPROPERTY(BlueprintReadWrite, Transient, Replicated)
	int32 NumFinish; //Count of finished orders

	UPROPERTY(BlueprintReadWrite, Transient, Replicated)
	int32 NumPerfect; //Count of finished orders in perfect time limit

	UPROPERTY(Transient, Replicated)
	int32 NumSave; //Count of finished orders that rest time is short

	UPROPERTY(BlueprintReadWrite)
	float BlockTime; //time of blocking others

	UPROPERTY(BlueprintReadWrite)
	uint8 bLosed : 1;

	UPROPERTY(BlueprintReadWrite) 
	uint8 bQuitter : 1;

public:

	UFUNCTION(BlueprintPure)
	FORCEINLINE int32 GetTeamID() { return TeamID; }

	UFUNCTION(BlueprintCallable)
	void SetTeamID(int32 NewTeamID);
};
