// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TABFood.h"
#include "TABHousehold.generated.h"



UCLASS()
class TAB_API ATABHousehold : public AActor
{
	GENERATED_BODY()

public:
	ATABHousehold();

	virtual void Tick(float delta) override;

	//就是多长时间执行一剩余时间的检测（。_。）
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Order")
	float CheckRateTime = 1.f;

	/**
	* 用来激活这个建筑，说明楼里有用户下单了

	* @param const TArray<ETABFoodType> 用户定的食物种类，可能是套餐（我看你TM就是在刁难我胖虎）
	* @param float 用户要求多少时间把餐送到
	* @param float 剩余多少时间之前送到就是5星好评
	* @param int32 是哪只队伍的订单，参数为队伍ID
	*/
	UFUNCTION(Category="Order", BlueprintCallable)
	void WaitForFood(const TArray<ETABFoodType>& FoodType, float LimitTime, float PerfectOrderTime, int32 FoodTeamID);
	

	UFUNCTION(Category = "Order", BlueprintCallable)
	bool IsWaittingForFood();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UTABInteractiveComponent* InteractiveComponent;

	UFUNCTION(Category = "Order", BlueprintPure)
	float GetTimeRemain();

	UFUNCTION(Category = "Order", BlueprintImplementableEvent)
	void OnDesireFood(int32 TeamID);

	UFUNCTION(Category = "Order", BlueprintImplementableEvent)
	void OnFinishOrder();

	UFUNCTION(Category = "Order", BlueprintImplementableEvent)
	void OnFailOrder();

	UFUNCTION(Category = "Order", BlueprintImplementableEvent)
	void OnGetFood();

	UPROPERTY(Category = "Order", BlueprintReadWrite)
	float PerfectTime;

	UPROPERTY(BlueprintReadOnly, Replicated)
	int32 TeamID;

	class ATABOrderGenerator* Company;
private:
	UPROPERTY()
	TArray<ETABFoodType> DesireFoods;

	UPROPERTY(Replicated)
	float TimeRemain;

	UPROPERTY()
	FTimerHandle TimeRemainHandle;

	/**
	* 每CheckRateTime调用一次
	*/
	void RemainTimeCheck();

	/**
	* 与Character进行交易(不，交互
	* @param ATABPlayerCharacter 交互对象
	*/
	UFUNCTION()
	void ReceiveFocus(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);

	UFUNCTION()
	void InteractWith(class UTABInteractiveComponent* Component, class ATABPlayerCharacter* PlayerCharacter);

	void GetFood(class ATABFood* Food);
	void PerfectOrder();
	void FailOrder();
	void FinishOrder();

	/**
	* 用来修改队伍的生命值，留有检测当前生命值的部分
	*
	* @param int32 改变多少（增加为正，减少为负）
	*/
	void ChangeTeamLife(int32 ChangeNum);

};
