// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/WidgetComponent.h"
#include "TABUIActor.generated.h"

/**
 * 
 */
UCLASS()
class TAB_API ATABUIActor : public AActor
{
	GENERATED_BODY()
	
	
public:
	ATABUIActor();

	void InteractWith(class ATABPlayerCharacter* PlayerCharacter);
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTABInteractiveComponent* InteractiveComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UWidgetComponent* WidgetComponent;
};
