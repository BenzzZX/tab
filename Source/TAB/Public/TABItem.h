// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TABItem.generated.h"

UCLASS()
class TAB_API ATABItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATABItem();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UI")
	FText Name;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void OnUse(class ATABPlayerCharacter* PlayerCharacter);

	UFUNCTION(BlueprintNativeEvent)
	bool CanPickBy(class ATABPlayerCharacter* PlayerCharacter);
	
};
