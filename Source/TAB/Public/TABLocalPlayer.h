// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LocalPlayer.h"
#include "TABLocalPlayer.generated.h"

/**
 * 
 */
UCLASS()
class TAB_API UTABLocalPlayer : public ULocalPlayer
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY()
	class UTABSaveGame* Persistent;
	
	void LoadPersistent();
	virtual FString GetNickname() const override;
};
