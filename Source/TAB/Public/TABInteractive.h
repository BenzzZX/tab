// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TABInteractive.generated.h"
class ATABPlayerCharacter;
class UTABInteractiveComponent;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInteractiveLoseFocusSignature, UTABInteractiveComponent*, InteractiveComponent, ATABPlayerCharacter*, PlayerPawn);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInteractiveReceiveFocusSignature, UTABInteractiveComponent*, InteractiveComponent, ATABPlayerCharacter*, PlayerPawn);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInteractiveInteractWithSignature, UTABInteractiveComponent*, InteractiveComponent, ATABPlayerCharacter*, PlayerPawn);

UCLASS(meta = (BlueprintSpawnableComponent))
class TAB_API UTABInteractiveComponent : public USceneComponent
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	UTABInteractiveComponent();

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FInteractiveReceiveFocusSignature OnReceiveFocus;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FInteractiveLoseFocusSignature OnLoseFocus;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FInteractiveInteractWithSignature OnInteractWith;

	void LoseFocus(ATABPlayerCharacter * PlayerPawn);
	void ReceiveFocus(ATABPlayerCharacter * PlayerPawn);
	void InteractWith(ATABPlayerCharacter* PlayerPawn);

	UFUNCTION(BlueprintCallable)
	void SetInstruction(const FText& NewInstruction);

	UFUNCTION(BlueprintPure)
	const FText& GetInstruction() const;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI", Replicated)
	bool bAcceptFocus;
private:
	UPROPERTY(EditAnywhere, Category = "UI", ReplicatedUsing=OnRep_Instruction)
	FText Instruction;

	TMap<class ATABPlayerCharacter*, int32> Overlapping;

	UFUNCTION()
	void OnRep_Instruction();

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};