// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "UserWidget.h"
#include "TABPlayerController.generated.h"


/**
 * 
 */
UCLASS()
class TAB_API ATABPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndplayReason) override;
	virtual void Tick(float DeltaSeconds) override;

	/** notify player about started match */
	UFUNCTION(reliable, client)
	void ClientGameStarted();

	/** Starts the online game using the session name in the PlayerState */
	UFUNCTION(reliable, client)
	void ClientStartOnlineGame();

	/** Ends the online game using the session name in the PlayerState */
	UFUNCTION(reliable, client)
	void ClientEndOnlineGame();

	void HandleReturnToRoom();

	UFUNCTION(reliable, client)
	void ClientSendRoundEndEvent(bool bIsWinner, int32 ExpendedTimeInSeconds);

	virtual void GameHasEnded(class AActor* EndGameFocus /* = NULL */, bool bIsWinner /* = false */) override;

	UFUNCTION(BlueprintNativeEvent)
	void HandleLose();

	UFUNCTION(BlueprintNativeEvent)
	void HandleWin();

	UPROPERTY(BlueprintReadOnly)
	float Ping;

	UPROPERTY(BlueprintReadOnly)
	float NetFPS;

	UPROPERTY(BlueprintReadOnly)
	float FPS;
private:
	/** Handle for efficient management of ClientStartOnlineGame timer */
	FTimerHandle TimerHandle_ClientStartOnlineGame;

	FTimerHandle TimerHandle_QueryNetworkQuality;

	UFUNCTION()
	void QueryNetworkQuality();
};
