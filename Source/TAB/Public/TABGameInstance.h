// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TABGameSession.h"
#include "UserWidget.h"
#include "FindSessionsCallbackProxy.h"
#include "TABGameInstance.generated.h"




class FTABPendingInvite
{
public:
	FTABPendingInvite() : ControllerId(-1), UserId(nullptr), bPrivilegesCheckedAndAllowed(false) {}

	int32							 ControllerId;
	TSharedPtr< const FUniqueNetId > UserId;
	FOnlineSessionSearchResult 		 InviteResult;
	bool							 bPrivilegesCheckedAndAllowed;
};

/**
 * 
 */
UCLASS()
class TAB_API UTABGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UTABGameInstance(const FObjectInitializer&);

	virtual void Init() override;
	virtual void Shutdown() override;
	virtual void StartGameInstance() override;

	ATABGameSession* GetGameSession() const;
	
	/** Travel directly to the named session */
	void TravelToSession(const FName& SessionName);

	UFUNCTION(BlueprintCallable, Meta = (DisplayName = "HostGame"))
	bool HostGameBP(APlayerController* LocalPlayer, const FString& Map, const FString& GameType, bool bIsLanMatch);

	UFUNCTION(BlueprintCallable, Meta = (DisplayName = "End Game"))
	void EndGameBP(APlayerController* LocalPlayer);

	UFUNCTION(BlueprintCallable, Meta = (DisplayName = "JoinSession"))
	bool JoinSessionBP(APlayerController* LocalPlayer, int32 SessionIndexInSearchResults);

	UFUNCTION(BlueprintCallable, Meta = (DisplayName = "FindSessions"))
	bool FindSessionsBP(APlayerController* LocalPlayer, bool bIsDedicatedServer, bool bIsLanMatch);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 bIsOnline : 1;

	UFUNCTION(BlueprintCallable)
	void FocusOnWidget(UUserWidget* Target);

	UFUNCTION(BlueprintCallable)
	void FocusOnGame();

	UFUNCTION(BlueprintPure, Category = "Session")
	void GetSessionSearchResults(TArray<FBlueprintSessionResult> &SearchResults);

	UFUNCTION(BlueprintPure, Category = "Session")
	void GetSessionSearchStatus(FString& Status);

	UPROPERTY(BlueprintReadOnly, Category = "UI")
	FText ErrorMessage;

	bool IsLocalPlayerOnline(ULocalPlayer* LocalPlayer);
	bool IsLocalPlayerSignedIn(ULocalPlayer* LocalPlayer);

	
	void CleanupSessionOnReturnToMenu();
private:
	bool Tick(float delta);

	void ShowError(const FText& ErrorMessage);

	void SetPresenceForLocalPlayers(const FString& StatusStr, const FVariantData& PresenceData);
	void OnCreatePresenceSessionComplete(FName SessionName, bool bWasSuccessful);
	void OnRegisterLocalPlayerComplete(const FUniqueNetId& PlayerId, EOnJoinSessionCompleteResult::Type Result);
	void FinishSessionCreation(EOnJoinSessionCompleteResult::Type Result);
	void TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString& ReasonString);
	void OnJoinSessionComplete(EOnJoinSessionCompleteResult::Type Result);
	void OnRegisterJoiningLocalPlayerComplete(const FUniqueNetId& PlayerId, EOnJoinSessionCompleteResult::Type Result);
	void FinishJoinSession(EOnJoinSessionCompleteResult::Type Result);
	void InternalTravelToSession(const FName& SessionName);
	void AddNetworkFailureHandlers();
	void RemoveNetworkFailureHandlers();
	void OnSearchSessionsComplete(bool bWasSuccessful);
	void OnEndSessionComplete(FName SessionName, bool bWasSuccessful);
	
	FString TravelURL;

	UPROPERTY(EditDefaultsOnly, Category = "UI", meta = (AllowPrivateAccess))
	TSubclassOf<UUserWidget> ErrorWindowClass;

	FDelegateHandle TravelLocalSessionFailureDelegateHandle;
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;
	FDelegateHandle OnSearchSessionsCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	FDelegateHandle OnEndSessionCompleteDelegateHandle;
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;
	FDelegateHandle OnCreatePresenceSessionCompleteDelegateHandle;

	FTickerDelegate TickDelegate;
	FOnEndSessionCompleteDelegate OnEndSessionCompleteDelegate;
	FDelegateHandle TickDelegateHandle;
};
