// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "UMG.h"
#include "ParticleDefinitions.h"
#include "SoundDefinitions.h"
#include "Net/UnrealNetwork.h"
#include "TABPlayerController.h"
#include "TABGameState.h"
#include "ParticleDefinitions.h"
#include "TABTypes.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTABGame, Log, All);

#define COLLISION_PICKUP ECC_GameTraceChannel1