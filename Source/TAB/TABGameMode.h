// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TABGameMode.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCountDownSignature, int32, Remain);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerReadySignature, int32, ReadyCount, int32, TotalCount);
/**
 * 
 */
UCLASS(Config = Game)
class TAB_API ATABGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATABGameMode();

	virtual void BeginPlay() override;
	void CountDown();
	void StartGame();
	void Reward(class ATABPlayerCharacter* PlayerPawn);
	void Punish(int32 TeamID);

	void RequestFinishAndExitToRoom();

	UFUNCTION(BlueprintCallable)
	virtual void ChangeTeamLife(int32 TeamID, int32 Delta);

	/** finish current match and lock players */
	UFUNCTION(exec, BlueprintCallable)
	void FinishMatch();

	UFUNCTION(BlueprintNativeEvent)
	void HandleShowWinnerAndRestart();

	UFUNCTION(BlueprintCallable)
	void TeamLose(int32 TeamID);

	UFUNCTION(BlueprintCallable)
	void TeamWin(int32 TeamID);

	void CheckReadyPlayerNum();

public:
	UFUNCTION(BlueprintCallable)
	void AddReadyPlayerNum();
	UFUNCTION(BlueprintCallable)
	void SubReadyPlayerNum();

	UPROPERTY(BlueprintAssignable)
	FCountDownSignature OnCountDown;

	UPROPERTY(BlueprintAssignable)
	FPlayerReadySignature OnPlayerReady;
protected:
	UPROPERTY(config)
	float TimeToShowWinner;

	UPROPERTY(config)
	int32 ReadyTime; //time before game really start

	UPROPERTY(config)
	int32 MaxLife; //max life of each team,reach this value lead to win

	UPROPERTY(config)
	int32 InitLife; //initial life of each team

	//Score multiplier to player states
	UPROPERTY(config)
	float FinishScore;

	UPROPERTY(config)
	float PerfectScore;

	UPROPERTY(config)
	float SaveScore;

	UPROPERTY(config)
	float BlockScore;

	UPROPERTY(config)
	float GrabScore;

	UPROPERTY(config)
	float CarryScore;


	/** Returns game session class to use */
	virtual TSubclassOf<AGameSession> GetGameSessionClass() const override;

private:

	void CheckTime();

	FTimerHandle GameStartTimer;
	
	int32 TeamRemain;

	FTimerHandle TimerHandle;
};