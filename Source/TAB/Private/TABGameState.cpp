// Fill out your copyright notice in the Description page of Project Settings.

#include "TABGameState.h"
#include "TABGameMode.h"
#include "TABGameInstance.h"
#include "TAB.h"
#include "TimerManager.h"



void ATABGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATABGameState, TeamLife);
	DOREPLIFETIME(ATABGameState, NumTeams);
	DOREPLIFETIME(ATABGameState, RemainTime);
	DOREPLIFETIME(ATABGameState, ReadyPlayerNum);
}

ATABGameState::ATABGameState()
{
	ReadyPlayerNum = 0;
	TeamLife.SetNumZeroed(4);
}

void ATABGameState::RequestFinishAndExitToRoom()
{
	if (AuthorityGameMode)
	{
		// we are server, tell the gamemode
		ATABGameMode* const GameMode = Cast<ATABGameMode>(AuthorityGameMode);
		if (GameMode)
		{
			GameMode->RequestFinishAndExitToRoom();
		}
	}
	else
	{
		// we are client, handle our own business
		UTABGameInstance* GameInstance = Cast<UTABGameInstance>(GetGameInstance());
		ATABPlayerController* const PrimaryPC = Cast<ATABPlayerController>(GameInstance->GetFirstLocalPlayerController());
		if (PrimaryPC)
		{
			check(PrimaryPC->GetNetMode() == ENetMode::NM_Client);
			PrimaryPC->HandleReturnToRoom();
		}
	}
}

void ATABGameState::OnRep_TeamLife()
{
	OnTeamLifeChange.Broadcast(TeamLife);
}


