// Fill out your copyright notice in the Description page of Project Settings.

#include "TABGameInstance.h"
#include "TAB.h"
#include "OnlineSubsystem.h"


UTABGameInstance::UTABGameInstance(const FObjectInitializer& ObjectInitlizer)
	:bIsOnline(true)
{
	
}

void UTABGameInstance::Init()
{
	Super::Init();
	TickDelegate = FTickerDelegate::CreateUObject(this, &UTABGameInstance::Tick);
	TickDelegateHandle = FTicker::GetCoreTicker().AddTicker(TickDelegate);
	OnEndSessionCompleteDelegate = FOnEndSessionCompleteDelegate::CreateUObject(this, &UTABGameInstance::OnEndSessionComplete);
}

void UTABGameInstance::Shutdown()
{
	Super::Shutdown();
	FTicker::GetCoreTicker().RemoveTicker(TickDelegateHandle);
}

void UTABGameInstance::StartGameInstance()
{
	Super::StartGameInstance();
}


void UTABGameInstance::FocusOnWidget(UUserWidget* Widget)
{
	UWorld* World = GetWorld();
	check(World);
	APlayerController* LocalController = World->GetFirstPlayerController();
	UE_LOG(LogOnline, Log, TEXT("Focus %s On Main Menu"), *LocalController->GetName());
	//UGameplayStatics::GetPlayerController(World, 0);
	if (LocalController)
	{
		FInputModeUIOnly InputMode;
		InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::LockOnCapture);
		if (IsValid(Widget))
		{
			InputMode.SetWidgetToFocus(Widget->TakeWidget());
		}
		LocalController->SetInputMode(InputMode);
	}
}

void UTABGameInstance::FocusOnGame()
{
	UWorld* World = GetWorld();
	check(World);
	APlayerController* LocalController = World->GetFirstPlayerController();
	UE_LOG(LogOnline, Log, TEXT("Focus %s On Game"), *LocalController->GetName());
	if (LocalController)
	{
		LocalController->SetInputMode(FInputModeGameOnly());
	}
}

void UTABGameInstance::GetSessionSearchStatus(FString& Status)
{
	int32 CurrentSearchIdx, NumSearchResults;
	ATABGameSession* const GameSession = GetGameSession();
	if (IsValid(GameSession))
		Status = EOnlineAsyncTaskState::ToString(GameSession->GetSearchResultStatus(CurrentSearchIdx, NumSearchResults));
	else
		Status = TEXT("NotStarted");
}

void UTABGameInstance::GetSessionSearchResults(TArray<FBlueprintSessionResult> &SearchResults)
{
	auto& Results = GetGameSession()->GetSearchResults();
	TArray<FBlueprintSessionResult> BPResults;
	for (auto& Result : Results)
	{
		FBlueprintSessionResult BPResult;
		BPResult.OnlineResult = Result;
		BPResults.Add(BPResult);
	}
	SearchResults = BPResults;
}


bool UTABGameInstance::Tick(float delta)
{
	//ATABPlayerPawn* PlayerPawn = Cast<ATABPlayerPawn>(GetFirstGamePlayer()->GetPlayerController()->GetPawn());
	return true;
}

void UTABGameInstance::ShowError(const FText& ErrorMessage)
{
	this->ErrorMessage = ErrorMessage;
	UUserWidget* ErrorWindow = CreateWidget<UUserWidget>(this, ErrorWindowClass);
	ErrorWindow->AddToViewport(0);
	FocusOnWidget(ErrorWindow);
}

void UTABGameInstance::SetPresenceForLocalPlayers(const FString& StatusStr, const FVariantData& PresenceData)
{
	const auto Presence = Online::GetPresenceInterface();
	if (Presence.IsValid())
	{
		for (int i = 0; i < LocalPlayers.Num(); ++i)
		{
			const TSharedPtr<const FUniqueNetId> UserId = LocalPlayers[i]->GetPreferredUniqueNetId();

			if (UserId.IsValid())
			{
				FOnlineUserPresenceStatus PresenceStatus;
				PresenceStatus.StatusStr = StatusStr;
				PresenceStatus.Properties.Add(DefaultPresenceKey, PresenceData);
				Presence->SetPresence(*UserId, PresenceStatus);
			}
		}
	}
}

void UTABGameInstance::OnCreatePresenceSessionComplete(FName SessionName, bool bWasSuccessful)
{
	ATABGameSession* const GameSession = GetGameSession();
	if (GameSession)
	{
		GameSession->OnCreatePresenceSessionComplete().Remove(OnCreatePresenceSessionCompleteDelegateHandle);
		// Add the splitscreen player if one exists
		if (bWasSuccessful && LocalPlayers.Num() > 1)
		{
			auto Sessions = Online::GetSessionInterface();
			if (Sessions.IsValid() && LocalPlayers[1]->GetPreferredUniqueNetId().IsValid())
			{
				Sessions->RegisterLocalPlayer(*LocalPlayers[1]->GetPreferredUniqueNetId(), GameSessionName,
					FOnRegisterLocalPlayerCompleteDelegate::CreateUObject(this, &UTABGameInstance::OnRegisterLocalPlayerComplete));
			}
		}
		else
		{
			// We either failed or there is only a single local user
			FinishSessionCreation(bWasSuccessful ? EOnJoinSessionCompleteResult::Success : EOnJoinSessionCompleteResult::UnknownError);
		}
	}
}

void UTABGameInstance::OnRegisterLocalPlayerComplete(const FUniqueNetId& PlayerId, EOnJoinSessionCompleteResult::Type Result)
{
	FinishSessionCreation(Result);
}

void UTABGameInstance::FinishSessionCreation(EOnJoinSessionCompleteResult::Type Result)
{
	if (Result == EOnJoinSessionCompleteResult::Success)
	{
		// This will send any Play Together invites if necessary, or do nothing.
		//SendPlayTogetherInvites();

		// Travel to the specified match URL
		GetWorld()->ServerTravel(TravelURL);
	}
	else
	{
		FText ReturnReason = NSLOCTEXT("NetworkErrors", "CreateSessionFailed", "Failed to create session.");
		SetPresenceForLocalPlayers(FString(TEXT("In Room")), FVariantData(FString(TEXT("InRoom"))));
		ShowError(ReturnReason);
	}
}

void UTABGameInstance::TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString& ReasonString)
{
	
	FText ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Join Session failed.");
	if (ReasonString.IsEmpty() == false)
	{
		ReturnReason = FText::Format(NSLOCTEXT("NetworkErrors", "JoinSessionFailedReasonFmt", "Join Session failed. {0}"),FText::FromString(ReasonString));
	}
	ShowError(ReturnReason);
}

void UTABGameInstance::AddNetworkFailureHandlers()
{
	// Add network/travel error handlers (if they are not already there)
	if (GEngine->OnTravelFailure().IsBoundToObject(this) == false)
	{
		TravelLocalSessionFailureDelegateHandle = GEngine->OnTravelFailure().AddUObject(this, &UTABGameInstance::TravelLocalSessionFailure);
	}
}

void UTABGameInstance::RemoveNetworkFailureHandlers()
{
	// Remove the local session/travel failure bindings if they exist
	if (GEngine->OnTravelFailure().IsBoundToObject(this) == true)
	{
		GEngine->OnTravelFailure().Remove(TravelLocalSessionFailureDelegateHandle);
	}
}


ATABGameSession* UTABGameInstance::GetGameSession() const
{
	UWorld* const World = GetWorld();
	if (World)
	{
		AGameModeBase* const Game = World->GetAuthGameMode();
		if (Game)
		{
			return Cast<ATABGameSession>(Game->GameSession);
		}
	}

	return nullptr;
}

void UTABGameInstance::FinishJoinSession(EOnJoinSessionCompleteResult::Type Result)
{
	if (Result != EOnJoinSessionCompleteResult::Success)
	{
		FText ReturnReason;
		switch (Result)
		{
		case EOnJoinSessionCompleteResult::SessionIsFull:
			ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Game is full.");
			break;
		case EOnJoinSessionCompleteResult::SessionDoesNotExist:
			ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Game no longer exists.");
			break;
		default:
			ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Join failed.");
			break;
		}

		FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");
		RemoveNetworkFailureHandlers();
		ShowError(ReturnReason);
		return;
	}

	InternalTravelToSession(GameSessionName);
}

void UTABGameInstance::InternalTravelToSession(const FName& SessionName)
{
	APlayerController * const PlayerController = GetFirstLocalPlayerController();

	if (PlayerController == nullptr)
	{
		FText ReturnReason = NSLOCTEXT("NetworkErrors", "InvalidPlayerController", "Invalid Player Controller");
		FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");
		RemoveNetworkFailureHandlers();
		ShowError(ReturnReason);
		return;
	}

	// travel to session
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub == nullptr)
	{
		FText ReturnReason = NSLOCTEXT("NetworkErrors", "OSSMissing", "OSS missing");
		FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");
		RemoveNetworkFailureHandlers();
		ShowError(ReturnReason);
		return;
	}

	FString URL;
	IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

	if (!Sessions.IsValid() || !Sessions->GetResolvedConnectString(SessionName, URL))
	{
		FText FailReason = NSLOCTEXT("NetworkErrors", "TravelSessionFailed", "Travel to Session failed.");
		ShowError(FailReason);
		UE_LOG(LogOnlineGame, Warning, TEXT("Failed to travel to session upon joining it"));
		return;
	}

	PlayerController->ClientTravel(URL, TRAVEL_Absolute);
}


void UTABGameInstance::OnRegisterJoiningLocalPlayerComplete(const FUniqueNetId& PlayerId, EOnJoinSessionCompleteResult::Type Result)
{
	FinishJoinSession(Result);
}

void UTABGameInstance::OnJoinSessionComplete(EOnJoinSessionCompleteResult::Type Result)
{
	// unhook the delegate
	ATABGameSession* const GameSession = GetGameSession();
	if (GameSession)
	{
		GameSession->OnJoinSessionComplete().Remove(OnJoinSessionCompleteDelegateHandle);
	}

	// Add the splitscreen player if one exists
	if (Result == EOnJoinSessionCompleteResult::Success && LocalPlayers.Num() > 1)
	{
		auto Sessions = Online::GetSessionInterface();
		if (Sessions.IsValid() && LocalPlayers[1]->GetPreferredUniqueNetId().IsValid())
		{
			Sessions->RegisterLocalPlayer(*LocalPlayers[1]->GetPreferredUniqueNetId(), GameSessionName,
				FOnRegisterLocalPlayerCompleteDelegate::CreateUObject(this, &UTABGameInstance::OnRegisterJoiningLocalPlayerComplete));
		}
	}
	else
	{
		// We either failed or there is only a single local user
		FinishJoinSession(Result);
	}
}


void UTABGameInstance::TravelToSession(const FName& SessionName)
{
	AddNetworkFailureHandlers();
	//ShowLoadingScreen();
	SetPresenceForLocalPlayers(FString(TEXT("In OnlineGame")), FVariantData(FString(TEXT("InOnlineGame"))));
	InternalTravelToSession(SessionName);
}

bool UTABGameInstance::HostGameBP(APlayerController* LocalPlayer, const FString& Map, const FString& GameType, bool bIsLanMatch)
{
	TravelURL = FString::Printf(TEXT("%s?game=%s%s%s?port=7777"), *Map, *GameType, bIsOnline ? TEXT("?listen") : TEXT(""), bIsLanMatch ? TEXT("?bIsLanMatch") : TEXT(""));

	if (!bIsOnline)
	{
		//
		// Offline game, just go straight to map
		//

		//ShowLoadingScreen();
		SetPresenceForLocalPlayers(FString(TEXT("In OnlineGame")), FVariantData(FString(TEXT("InOnlineGame"))));
		GetWorld()->ServerTravel(TravelURL);
		return true;
	}

	ATABGameSession* const GameSession = GetGameSession();
	if (GameSession)
	{
		OnCreatePresenceSessionCompleteDelegateHandle = GameSession->OnCreatePresenceSessionComplete().
			AddUObject(this, &UTABGameInstance::OnCreatePresenceSessionComplete);

		// determine the map name from the travelURL
		const FString& MapNameSubStr = "/Game/Maps/";
		const FString& ChoppedMapName = TravelURL.RightChop(MapNameSubStr.Len());
		const FString& MapName = ChoppedMapName.LeftChop(ChoppedMapName.Len() - ChoppedMapName.Find("?game"));

		if (GameSession->HostSession(LocalPlayer->PlayerState->UniqueId.GetUniqueNetId(), GameSessionName, GameType, MapName, bIsLanMatch, true, ATABGameSession::DEFAULT_NUM_PLAYERS))
		{
			// Go ahead and go into loading state now
			// If we fail, the delegate will handle showing the proper messaging and move to the correct state
			//ShowLoadingScreen();
			SetPresenceForLocalPlayers(FString(TEXT("In OnlineGame")), FVariantData(FString(TEXT("InOnlineGame"))));
			return true;
		}
	}
	return false;
}

void UTABGameInstance::EndGameBP(APlayerController* LocalPlayer)
{
	// Clear the players' presence information
	SetPresenceForLocalPlayers(FString(TEXT("In Menu")), FVariantData(FString(TEXT("OnMenu"))));

	UWorld* const World = GetWorld();
	ATABGameState* const GameState = World != NULL ? World->GetGameState<ATABGameState>() : NULL;

	if (GameState)
	{
		// Send round end events for local players
		for (int i = 0; i < LocalPlayers.Num(); ++i)
		{
			auto TABPC = Cast<ATABPlayerController>(LocalPlayers[i]->PlayerController);
			if (TABPC)
			{
				// Assuming you can't win if you quit early
				TABPC->ClientSendRoundEndEvent(false, GameState->ElapsedTime);
			}
		}

		// Give the game state a chance to cleanup first
		GameState->RequestFinishAndExitToRoom();
	}
	else
	{
		// If there is no game state, make sure the session is in a good state
		CleanupSessionOnReturnToMenu();
	}

}

bool UTABGameInstance::JoinSessionBP(APlayerController* LocalPlayer, int32 SessionIndexInSearchResults)
{
	ATABGameSession* const GameSession = GetGameSession();
	if (GameSession)
	{
		AddNetworkFailureHandlers();

		OnJoinSessionCompleteDelegateHandle = GameSession->OnJoinSessionComplete().AddUObject(this, &UTABGameInstance::OnJoinSessionComplete);
		if (GameSession->JoinSession(LocalPlayer->PlayerState->UniqueId.GetUniqueNetId(), GameSessionName, GetGameSession()->GetSearchResults()[SessionIndexInSearchResults]))
		{
			//ShowLoadingScreen();
			SetPresenceForLocalPlayers(FString(TEXT("In OnlineGame")), FVariantData(FString(TEXT("InOnlineGame"))));
			return true;
		}
	}

	return false;
}

bool UTABGameInstance::FindSessionsBP(APlayerController* LocalPlayer, bool bIsDedicatedServer, bool bIsLANMatch)
{
	bool bResult = false;

	check(LocalPlayer != nullptr);
	if (LocalPlayer)
	{
		ATABGameSession* const GameSession = GetGameSession();
		if (GameSession)
		{
			GameSession->OnFindSessionsComplete().RemoveAll(this);
			OnSearchSessionsCompleteDelegateHandle = GameSession->OnFindSessionsComplete().AddUObject(this, &UTABGameInstance::OnSearchSessionsComplete);

			GameSession->FindSessions(LocalPlayer->PlayerState->UniqueId.GetUniqueNetId(), GameSessionName, bIsLANMatch, !bIsDedicatedServer);

			bResult = true;
		}
	}

	return bResult;
}

/** Callback which is intended to be called upon finding sessions */
void UTABGameInstance::OnSearchSessionsComplete(bool bWasSuccessful)
{
	ATABGameSession* const Session = GetGameSession();
	if (Session)
	{
		Session->OnFindSessionsComplete().Remove(OnSearchSessionsCompleteDelegateHandle);
	}
}


bool UTABGameInstance::IsLocalPlayerOnline(ULocalPlayer* LocalPlayer)
{
	if (LocalPlayer == NULL)
	{
		return false;
	}
	const auto OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		const auto IdentityInterface = OnlineSub->GetIdentityInterface();
		if (IdentityInterface.IsValid())
		{
			auto UniqueId = LocalPlayer->GetCachedUniqueNetId();
			if (UniqueId.IsValid())
			{
				const auto LoginStatus = IdentityInterface->GetLoginStatus(*UniqueId);
				if (LoginStatus == ELoginStatus::LoggedIn)
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool UTABGameInstance::IsLocalPlayerSignedIn(ULocalPlayer* LocalPlayer)
{
	if (LocalPlayer == NULL)
	{
		return false;
	}

	const auto OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		const auto IdentityInterface = OnlineSub->GetIdentityInterface();
		if (IdentityInterface.IsValid())
		{
			auto UniqueId = LocalPlayer->GetCachedUniqueNetId();
			if (UniqueId.IsValid())
			{
				return true;
			}
		}
	}

	return false;
}

void UTABGameInstance::OnEndSessionComplete(FName SessionName, bool bWasSuccessful)
{
	UE_LOG(LogOnline, Log, TEXT("UTABGameInstance::OnEndSessionComplete: Session=%s bWasSuccessful=%s"), *SessionName.ToString(), bWasSuccessful ? TEXT("true") : TEXT("false"));

	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			Sessions->ClearOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegateHandle);
			Sessions->ClearOnEndSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegateHandle);
			Sessions->ClearOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegateHandle);
		}
	}

	// continue
	CleanupSessionOnReturnToMenu();
}

void UTABGameInstance::CleanupSessionOnReturnToMenu()
{
	bool bPendingOnlineOp = false;

	// end online game and then destroy it
	IOnlineSubsystem * OnlineSub = IOnlineSubsystem::Get();
	IOnlineSessionPtr Sessions = (OnlineSub != NULL) ? OnlineSub->GetSessionInterface() : NULL;

	if (Sessions.IsValid())
	{
		EOnlineSessionState::Type SessionState = Sessions->GetSessionState(GameSessionName);
		UE_LOG(LogOnline, Log, TEXT("Session %s is '%s'"), *GameSessionName.ToString(), EOnlineSessionState::ToString(SessionState));

		if (EOnlineSessionState::InProgress == SessionState)
		{
			UE_LOG(LogOnline, Log, TEXT("Ending session %s on return to main menu"), *GameSessionName.ToString());
			OnEndSessionCompleteDelegateHandle = Sessions->AddOnEndSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
			Sessions->EndSession(GameSessionName);
			bPendingOnlineOp = true;
		}
		else if (EOnlineSessionState::Ending == SessionState)
		{
			UE_LOG(LogOnline, Log, TEXT("Waiting for session %s to end on return to main menu"), *GameSessionName.ToString());
			OnEndSessionCompleteDelegateHandle = Sessions->AddOnEndSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
			bPendingOnlineOp = true;
		}
		else if (EOnlineSessionState::Ended == SessionState || EOnlineSessionState::Pending == SessionState)
		{
			UE_LOG(LogOnline, Log, TEXT("Destroying session %s on return to main menu"), *GameSessionName.ToString());
			OnDestroySessionCompleteDelegateHandle = Sessions->AddOnDestroySessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
			Sessions->DestroySession(GameSessionName);
			bPendingOnlineOp = true;
		}
		else if (EOnlineSessionState::Starting == SessionState)
		{
			UE_LOG(LogOnline, Log, TEXT("Waiting for session %s to start, and then we will end it to return to main menu"), *GameSessionName.ToString());
			OnStartSessionCompleteDelegateHandle = Sessions->AddOnStartSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
			bPendingOnlineOp = true;
		}
	}

	if (!bPendingOnlineOp)
	{
		//GEngine->HandleDisconnect( GetWorld(), GetWorld()->GetNetDriver() );
	}
}
