// Fill out your copyright notice in the Description page of Project Settings.

#include "TABOrderGenerator.h"
#include "Kismet/GameplayStatics.h"
#include "TABHousehold.h"
#include "TABRestaurant.h"
#include "TABInteractive.h"
#include "TABPlayerCharacter.h"
#include "TABGameMode.h"
#include "TAB.h"


// Sets default values
ATABOrderGenerator::ATABOrderGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned

void ATABOrderGenerator::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(this, ATABHousehold::StaticClass(), AvailableHouseholds);
	for (auto Actor : AvailableHouseholds)
	{
		if (auto House = Cast<ATABHousehold>(Actor))
		{
			House->Company = this;
		}
	}
	UGameplayStatics::GetAllActorsOfClass(this, ATABRestaurant::StaticClass(), AvailableRestaurants);
	for (auto Actor : AvailableRestaurants)
	{
		if (auto Restaurant = Cast<ATABRestaurant>(Actor))
		{
			Restaurant->Company = this;
		}
	}
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &ATABOrderGenerator::OnGenerate, 2.0f, false);
}

void ATABOrderGenerator::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
}

// Called every frame
void ATABOrderGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool ATABOrderGenerator::GetATeam(int32& TeamID)
{
	int32 Teams[10], k=0;
	if (ATABGameState* GameState = Cast<ATABGameState>(GetWorld()->GetGameState()))
	{
		for (int32 i = 0; i < GameState->NumTeams; i++)
		{
			if (GameState->TeamLife[i] > 0)
			{
				Teams[k++] = i;
				UE_LOG(LogTABGame, Log, TEXT("Team Alive %d"), i);
			}
		}
	}
	if (k > 0)
	{
		TeamID = Teams[FMath::RandHelper(k)];
		
		return true;
	}
	else return false;
}

void ATABOrderGenerator::OnGenerate()
{
	int32 TeamID;
	if (AvailableHouseholds.Num() <= 0) return;
	if (AvailableRestaurants.Num() <= 0) return;

	//选中一支队伍
	if (!GetATeam(TeamID)) return;

	UE_LOG(LogTABGame, Log, TEXT("Select Team %d"), TeamID);
	//选中一家住户
	int randIndex = FMath::RandHelper(AvailableHouseholds.Num());
	ATABHousehold *household = Cast<ATABHousehold>(AvailableHouseholds[randIndex]);
	AvailableHouseholds.RemoveAtSwap(randIndex);

	//选中一家餐厅
	randIndex = FMath::RandHelper(AvailableRestaurants.Num());
	ATABRestaurant *restaurant = Cast<ATABRestaurant>(AvailableRestaurants[randIndex]);
	AvailableRestaurants.RemoveAtSwap(randIndex);

	//生成订单
	TArray<ETABFoodType> FoodToOrder;
	FoodToOrder.Add(restaurant->FoodInfo.Type);
	
	
	household->WaitForFood(FoodToOrder, OrderLimitTime, OrderPerfectTime, TeamID);
	restaurant->ProductFood(TeamID);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &ATABOrderGenerator::OnGenerate, Interval, false);
}

void ATABOrderGenerator::RegisterHousehold(class ATABHousehold* Household)
{ 
	AvailableHouseholds.Add(Household);
}

void ATABOrderGenerator::RegisterRestaurant(class ATABRestaurant* Restaurant)
{
	AvailableRestaurants.Add(Restaurant);
}

