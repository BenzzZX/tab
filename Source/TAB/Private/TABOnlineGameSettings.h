// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "OnlineSessionSettings.h"
/**
 * General session settings for a TAB game
 */
class FTABOnlineSessionSettings : public FOnlineSessionSettings
{
public:

	FTABOnlineSessionSettings(bool bIsLAN = false, bool bIsPresence = false, int32 MaxNumPlayers = 4);
	virtual ~FTABOnlineSessionSettings() {}
};

/**
 * General search setting for a TAB game
 */
class FTABOnlineSearchSettings : public FOnlineSessionSearch
{
public:
	FTABOnlineSearchSettings(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FTABOnlineSearchSettings() {}
};

/**
 * Search settings for an empty dedicated server to host a match
 */
class FTABOnlineSearchSettingsEmptyDedicated : public FTABOnlineSearchSettings
{
public:
	FTABOnlineSearchSettingsEmptyDedicated(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FTABOnlineSearchSettingsEmptyDedicated() {}
};
