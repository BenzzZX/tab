// Fill out your copyright notice in the Description page of Project Settings.

#include "TABRestaurant.h"
#include "TABInteractive.h"
#include "TABFood.h"
#include "TABOrderGenerator.h"
#include "Kismet/GameplayStatics.h"
#include "TABPlayerState.h"
#include "TABPlayerCharacter.h"

ATABRestaurant::ATABRestaurant()
{
	PrimaryActorTick.bCanEverTick = false;
	SetReplicates(true);
	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	InteractiveComponent = CreateDefaultSubobject<UTABInteractiveComponent>("InteractiveComponent");
	InteractiveComponent->SetupAttachment(RootComponent);
	InteractiveComponent->OnInteractWith.AddDynamic(this, &ATABRestaurant::InteractWith);
	InteractiveComponent->OnReceiveFocus.AddDynamic(this, &ATABRestaurant::ReceiveFocus);
}

void ATABRestaurant::ProductFood(int32 TeamID)
{
	ATABFood* Food = Cast<ATABFood>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, FoodClass, GetActorTransform()));
	if (IsValid(Food))
	{
		Food->Info = FoodInfo;
		Food->TimeToBeOverdue = FoodInfo.ShelfLife;
		Food->TeamID = TeamID;
		UGameplayStatics::FinishSpawningActor(Food, GetActorTransform());
		//Food->AttachToActor(this, FAttachmentTransformRules::SnapToTargetIncludingScale);
		
	}
	TArray<AActor*> Overlap;
	GetOverlappingActors(Overlap,ATABPlayerCharacter::StaticClass());
	FoodReady = Food;
	OnProductFood();
	for (auto actor : Overlap)
	{
		if (auto Character = Cast<ATABPlayerCharacter>(actor))
		{
			Character->UpdateFocusing();
		}
	}
}

void ATABRestaurant::GiveFood(class ATABPlayerCharacter* PlayerPawn)
{
	if (IsValid(FoodReady))
	{
		ATABPlayerState* PlayerState = Cast<ATABPlayerState>(PlayerPawn->PlayerState);
		PlayerPawn->TakeItem(FoodReady);
		InteractiveComponent->bAcceptFocus = false;
		FoodReady = NULL;
	}
	if(IsValid(Company)) Company->RegisterRestaurant(this);
}

void ATABRestaurant::ReceiveFocus(UTABInteractiveComponent* Component, ATABPlayerCharacter* PlayerCharacter)
{
	ATABPlayerState* CharacterPlayerState = Cast<ATABPlayerState>(PlayerCharacter->PlayerState);
	ATABFood* FoodCarrying = PlayerCharacter->FoodCarrying;
	InteractiveComponent->bAcceptFocus = IsValid(FoodReady) && /*IsValid(CharacterPlayerState) && CharacterPlayerState->GetTeamID() == FoodReady->TeamID &&*/ !IsValid(FoodCarrying);
}

void ATABRestaurant::InteractWith(UTABInteractiveComponent* Component, ATABPlayerCharacter* PlayerPawn)
{
	GiveFood(PlayerPawn);
}
