// Fill out your copyright notice in the Description page of Project Settings.

#include "TABInteractive.h"
#include "TABPlayerCharacter.h"
#include "TAB.h"

UTABInteractiveComponent::UTABInteractiveComponent()
{
	Instruction = FText::GetEmpty();
	bAcceptFocus = true;
	SetIsReplicated(true);
}



void UTABInteractiveComponent::BeginPlay()
{
	TArray<USceneComponent *> childComponents;
	GetChildrenComponents(true, childComponents);
	for (USceneComponent *childComponent : childComponents)
	{
		if (UPrimitiveComponent* primitiveChild = Cast<UPrimitiveComponent>(childComponent))
		{
			UE_LOG(LogTABGame, Log, TEXT("%s Register child %s"), *this->GetName(), *primitiveChild->GetName());
			primitiveChild->OnComponentBeginOverlap.AddDynamic(this, &UTABInteractiveComponent::OnBeginOverlap);
			primitiveChild->OnComponentEndOverlap.AddDynamic(this, &UTABInteractiveComponent::OnEndOverlap);
		}
	}
}

void UTABInteractiveComponent::LoseFocus(ATABPlayerCharacter * PlayerPawn)
{
	OnLoseFocus.Broadcast(this, PlayerPawn);
}

void UTABInteractiveComponent::ReceiveFocus(ATABPlayerCharacter * PlayerPawn)
{
	OnReceiveFocus.Broadcast(this, PlayerPawn);
	
}

void UTABInteractiveComponent::InteractWith(ATABPlayerCharacter * PlayerPawn)
{
	OnInteractWith.Broadcast(this, PlayerPawn);
}

void UTABInteractiveComponent::SetInstruction(const FText& NewInstruction)
{
	Instruction = NewInstruction;
	OnRep_Instruction();
}

const FText& UTABInteractiveComponent::GetInstruction() const
{
	return Instruction;
}

void UTABInteractiveComponent::OnRep_Instruction()
{
	Cast<ATABPlayerCharacter>(UGameplayStatics::GetPlayerPawn(this, 0))->OnChangeStatus.Broadcast();
}

void UTABInteractiveComponent::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ATABPlayerCharacter *PlayerPawn = Cast<ATABPlayerCharacter>(OtherActor))
	{
		Overlapping.FindOrAdd(PlayerPawn) += 1;
		if (Overlapping.FindRef(PlayerPawn) == 1) PlayerPawn->NotifyEnterInteractive(this);
	}
}

void UTABInteractiveComponent::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (ATABPlayerCharacter *PlayerPawn = Cast<ATABPlayerCharacter>(OtherActor))
	{
		Overlapping.FindOrAdd(PlayerPawn) -= 1;
		if (Overlapping.FindRef(PlayerPawn) == 0) PlayerPawn->NotifyLeaveInteractive(this);
	}
}


void UTABInteractiveComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UTABInteractiveComponent, bAcceptFocus);
	DOREPLIFETIME(UTABInteractiveComponent, Instruction);
}