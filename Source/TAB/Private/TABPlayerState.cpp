// Fill out your copyright notice in the Description page of Project Settings.

#include "TABPlayerState.h"
#include "TAB.h"
#include "TimerManager.h"
#include "TABPlayerCharacter.h"

void ATABPlayerState::UpdateTeamColor()
{
	AController* OwnerController = Cast<AController>(GetOwner());
	if (OwnerController != NULL)
	{
		ATABPlayerCharacter* TABCharacter = Cast<ATABPlayerCharacter>(OwnerController->GetCharacter());
		if (TABCharacter != NULL)
		{
			TABCharacter->UpdateTeamColor(TeamID);
		}
	}
}

void ATABPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATABPlayerState, TeamID);
	DOREPLIFETIME(ATABPlayerState, NumFinish);
	DOREPLIFETIME(ATABPlayerState, NumPerfect);
	DOREPLIFETIME(ATABPlayerState, NumSave);
}

void ATABPlayerState::SetTeamID(int32 TeamNum)
{
	TeamID = TeamNum;
	UpdateTeamColor();
}

