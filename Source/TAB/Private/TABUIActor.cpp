// Fill out your copyright notice in the Description page of Project Settings.

#include "TABUIActor.h"
#include "TABInteractive.h"
#include "TABPlayerCharacter.h"


ATABUIActor::ATABUIActor()
{
	PrimaryActorTick.bCanEverTick = false;
	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	InteractiveComponent = CreateDefaultSubobject<UTABInteractiveComponent>("InteractiveComponent");
	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>("WidgetComponent");
	WidgetComponent->SetupAttachment(RootComponent);
	InteractiveComponent->SetupAttachment(RootComponent);
}

void ATABUIActor::InteractWith(ATABPlayerCharacter* PlayerCharacter)
{

}
