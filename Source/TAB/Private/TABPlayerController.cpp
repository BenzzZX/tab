// Fill out your copyright notice in the Description page of Project Settings.

#include "TABPlayerController.h"
#include "TABPlayerState.h"
#include "TABGameInstance.h"
#include "TABGameState.h"
#include "Online.h"
#include "OnlineAchievementsInterface.h"
#include "OnlineEventsInterface.h"
#include "OnlineIdentityInterface.h"
#include "OnlineSessionInterface.h"
#include "Misc/PackageName.h"
#include "TAB.h"

void ATABPlayerController::ClientGameStarted_Implementation()
{

}

void ATABPlayerController::ClientStartOnlineGame_Implementation()
{
	ATABPlayerState* TABPlayerState = Cast<ATABPlayerState>(PlayerState);
	if (TABPlayerState)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid())
			{
				UE_LOG(LogOnline, Log, TEXT("Starting session %s on client"), *TABPlayerState->SessionName.ToString());
				Sessions->StartSession(TABPlayerState->SessionName);
			}
		}
	}
	else
	{
		// Keep retrying until player state is replicated
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ClientStartOnlineGame, this, &ATABPlayerController::ClientStartOnlineGame_Implementation, 0.2f, false);
	}
}

void ATABPlayerController::ClientEndOnlineGame_Implementation()
{
	ATABPlayerState* TABPlayerState = Cast<ATABPlayerState>(PlayerState);
	if (TABPlayerState)
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid())
			{
				UE_LOG(LogOnline, Log, TEXT("Ending session %s on client"), *TABPlayerState->SessionName.ToString());
				Sessions->EndSession(TABPlayerState->SessionName);
			}
		}
	}
}

void ATABPlayerController::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(TimerHandle_QueryNetworkQuality, this, &ATABPlayerController::QueryNetworkQuality, 0.5f, true, 1.f);
}

void ATABPlayerController::EndPlay(EEndPlayReason::Type EndplayReason)
{
	Super::EndPlay(EndplayReason);
	GetWorldTimerManager().ClearTimer(TimerHandle_QueryNetworkQuality);
}

void ATABPlayerController::Tick(float DeltaSeconds)
{
	FPS = 1.f / DeltaSeconds;
}

void ATABPlayerController::HandleReturnToRoom()
{
	UTABGameInstance * SGI = GetWorld() != NULL ? Cast<UTABGameInstance>(GetWorld()->GetGameInstance()) : NULL;

	if (ensure(SGI != NULL))
	{
		SGI->CleanupSessionOnReturnToMenu();
	}
}

void ATABPlayerController::GameHasEnded(class AActor* EndGameFocus /* = NULL */, bool bIsWinner /* = false */)
{
	if (bIsWinner)
	{
		HandleWin();
	}
	else
	{
		HandleLose();
	}
}

void ATABPlayerController::QueryNetworkQuality()
{
	UNetConnection* NetConnection = GetNetConnection();
	if (!NetConnection) return;
	Ping = NetConnection->AvgLag * 1000.f;
	NetFPS = 1.f / NetConnection->AverageFrameTime;
}

void ATABPlayerController::HandleLose_Implementation()
{

}

void ATABPlayerController::HandleWin_Implementation()
{

}

void ATABPlayerController::ClientSendRoundEndEvent_Implementation(bool bIsWinner, int32 ExpendedTimeInSeconds)
{
	/*
	const auto Events = Online::GetEventsInterface();
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (//bHasSentStartEvents &&
		LocalPlayer != nullptr && Events.IsValid())
	{
		auto UniqueId = LocalPlayer->GetPreferredUniqueNetId();

		if (UniqueId.IsValid())
		{
			FString MapName = *FPackageName::GetShortName(GetWorld()->PersistentLevel->GetOutermost()->GetName());
			ATABPlayerState* TABPlayerState = Cast<ATABPlayerState>(PlayerState);
			int32 PlayerScore = TABPlayerState ? TABPlayerState->Score : 0;

			// Fire session end event for all cases
			FOnlineEventParms Params;
			Params.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
			Params.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused
			Params.Add(TEXT("ExitStatusId"), FVariantData((int32)0)); // unused
			Params.Add(TEXT("PlayerScore"), FVariantData((int32)PlayerScore));
			Params.Add(TEXT("PlayerWon"), FVariantData((bool)bIsWinner));
			Params.Add(TEXT("MapName"), FVariantData(MapName));
			Params.Add(TEXT("MapNameString"), FVariantData(MapName)); // @todo workaround for a bug in backend service, remove when fixed

			Events->TriggerEvent(*UniqueId, TEXT("PlayerSessionEnd"), Params);

			// Online matches require the MultiplayerRoundEnd event as well
			UTABGameInstance* SGI = GetWorld() != NULL ? Cast<UTABGameInstance>(GetWorld()->GetGameInstance()) : NULL;
			if (SGI->bIsOnline)
			{
				FOnlineEventParms MultiplayerParams;

				ATABGameState* const MyGameState = GetWorld() != NULL ? GetWorld()->GetGameState<ATABGameState>() : NULL;
				if (ensure(MyGameState != nullptr))
				{
					MultiplayerParams.Add(TEXT("SectionId"), FVariantData((int32)0)); // unused
					MultiplayerParams.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ffa v tdm)
					MultiplayerParams.Add(TEXT("MatchTypeId"), FVariantData((int32)1)); // @todo abstract the specific meaning of this value across platforms
					MultiplayerParams.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused
					MultiplayerParams.Add(TEXT("TimeInSeconds"), FVariantData((float)ExpendedTimeInSeconds));
					MultiplayerParams.Add(TEXT("ExitStatusId"), FVariantData((int32)0)); // unused

					Events->TriggerEvent(*UniqueId, TEXT("MultiplayerRoundEnd"), MultiplayerParams);
				}
			}
		}

		//bHasSentStartEvents = false;
	}*/
}


