// Fill out your copyright notice in the Description page of Project Settings.

#include "TABUserSettings.h"




void UTABUserSettings::SetToDefaults()
{
	Super::SetToDefaults();
}

void UTABUserSettings::ApplySettings(bool bCheckForCommandLineOverrides)
{
	Super::ApplySettings(bCheckForCommandLineOverrides);
}
