// Fill out your copyright notice in the Description page of Project Settings.

#include "TABHousehold.h"
#include "TABInteractive.h"
#include "TimerManager.h"
#include "TABPlayerCharacter.h"
#include "TABPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "TABGameState.h"
#include "TABGameMode.h"
#include "TABOrderGenerator.h"
#include "TAB.h"
#include "TABFood.h"

ATABHousehold::ATABHousehold()
{
	PrimaryActorTick.bCanEverTick = false;
	SetReplicates(true);
	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	InteractiveComponent = CreateDefaultSubobject<UTABInteractiveComponent>("InteractiveComponent");
	InteractiveComponent->SetupAttachment(RootComponent);
	InteractiveComponent->OnReceiveFocus.AddDynamic(this, &ATABHousehold::ReceiveFocus);
	InteractiveComponent->OnInteractWith.AddDynamic(this, &ATABHousehold::InteractWith);
}

void ATABHousehold::Tick(float delta)
{
	Super::Tick(delta);
}

void ATABHousehold::RemainTimeCheck()
{
	TimeRemain -= CheckRateTime;
	if (TimeRemain <= 0)
	{
		FailOrder();
	}
}

void ATABHousehold::WaitForFood(const TArray<ETABFoodType>& FoodType, float LimitTime, float PerfectOrderTime, int32 FoodTeamID)
{
	TimeRemain = LimitTime;
	DesireFoods = FoodType;
	PerfectTime = PerfectOrderTime;
	TeamID = FoodTeamID;
	OnDesireFood(TeamID);
	GetWorldTimerManager().SetTimer(TimeRemainHandle, this, &ATABHousehold::RemainTimeCheck, CheckRateTime, true);
}

bool ATABHousehold::IsWaittingForFood()
{
	return DesireFoods.Num()>0;
}

float ATABHousehold::GetTimeRemain()
{
	return TimeRemain;
}

void ATABHousehold::GetFood(ATABFood* Food)
{
	
	FTABFoodInfo Info = Food->Info;
	if (Food->bIsOverdue && FMath::RandBool())
	{
		FailOrder();
		Food->Destroy();
		return;
	}
	DesireFoods.RemoveSingleSwap(Info.Type);
	if (DesireFoods.Num() <= 0)
	{
		if (TimeRemain >= PerfectTime)
			PerfectOrder();
		FinishOrder();
		InteractiveComponent->bAcceptFocus = false;
		Food->Destroy();
		return;
	}
	OnGetFood();
	Food->Destroy();
}

void ATABHousehold::FailOrder()
{
	GetWorldTimerManager().ClearTimer(TimeRemainHandle);
	if (IsValid(Company)) Company->RegisterHousehold(this);
	OnFailOrder();
	ChangeTeamLife(-1);
}

void ATABHousehold::PerfectOrder()
{
	GetWorldTimerManager().ClearTimer(TimeRemainHandle);
	if (IsValid(Company)) Company->RegisterHousehold(this);
	OnFinishOrder();
	ChangeTeamLife(1);
}

void ATABHousehold::FinishOrder()
{
	GetWorldTimerManager().ClearTimer(TimeRemainHandle);
	if (IsValid(Company)) Company->RegisterHousehold(this);
	OnFinishOrder();
}

void ATABHousehold::ReceiveFocus(UTABInteractiveComponent* Component, ATABPlayerCharacter* PlayerCharacter)
{
	ATABPlayerState* CharacterPlayerState = Cast<ATABPlayerState>(PlayerCharacter->PlayerState);
	ATABFood* FoodCarrying = PlayerCharacter->FoodCarrying;
	InteractiveComponent->bAcceptFocus = IsValid(CharacterPlayerState) && CharacterPlayerState->GetTeamID() == TeamID && IsValid(FoodCarrying) && DesireFoods.Contains(FoodCarrying->Info.Type);
}


void ATABHousehold::InteractWith(UTABInteractiveComponent* Component,ATABPlayerCharacter* PlayerCharacter)
{
	ATABFood* FoodCarrying = PlayerCharacter->GiveFood();
	if(IsValid(FoodCarrying)) GetFood(FoodCarrying);
}

void ATABHousehold::ChangeTeamLife(int32 ChangeNum)
{
	ATABGameMode* TABGameMode = Cast<ATABGameMode>(UGameplayStatics::GetGameMode(this));
	if (IsValid(TABGameMode)) TABGameMode->ChangeTeamLife(TeamID, ChangeNum);
}

void ATABHousehold::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATABHousehold, TimeRemain);
	DOREPLIFETIME(ATABHousehold, TeamID);
}