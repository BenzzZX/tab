// Fill out your copyright notice in the Description page of Project Settings.

#include "TABCharacterMovementComponent.h"
#include "TABPlayerCharacter.h"
#include "TAB.h"

UTABCharacterMovementComponent::UTABCharacterMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UTABCharacterMovementComponent::UpdateFromCompressedFlags(uint8 Flags)
{
	Super::UpdateFromCompressedFlags(Flags);
	bIsJetting = (Flags&FSavedMove_Character::FLAG_Custom_0) != 0;
}

class FNetworkPredictionData_Client* UTABCharacterMovementComponent::GetPredictionData_Client() const
{
	check(PawnOwner != nullptr);
	//check(PawnOwner->Role < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UTABCharacterMovementComponent* MutableThis = const_cast<UTABCharacterMovementComponent*>(this);
		MutableThis->ClientPredictionData = new class FNetworkPredictionData_Client_TABCharacter(*this);
	}
	ClientPredictionData->MaxSmoothNetUpdateDist = bIsJetting ? 300.f : 92.f;
	ClientPredictionData->NoSmoothNetUpdateDist = bIsJetting ? 500.f : 140.f;
	return ClientPredictionData;
}

void UTABCharacterMovementComponent::PerformMovement(float DeltaTime)
{
	ATABPlayerCharacter* Character = Cast<ATABPlayerCharacter>(CharacterOwner);
	Super::PerformMovement(DeltaTime);
}

void UTABCharacterMovementComponent::OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);
	FVector Accel = FVector::ZeroVector;
	ATABPlayerCharacter* Character = Cast<ATABPlayerCharacter>(CharacterOwner);
	do
	{
		if (!bIsJetting)
			break;
		if (FuelLeft < KINDA_SMALL_NUMBER)
		{
			Character->StopJetting();
			bIsJetting = false;
			break;
		}
		FuelLeft -= FuelBurnRate * DeltaSeconds;
		Facing.Normalize();
		Facing.Z = 0.f;
		
		if (IsMovingOnGround())
			Accel += Facing * JetStrength * DeltaSeconds;
		else
		{
			float ZVelocity = FMath::Min(Velocity.Z, 0.f);
			float ZTrend = -ZVelocity;
			float JetVel = JetStrength*DeltaSeconds*(1-JetUpRatio);
			float ForwardStrengthSquared = FMath::Square(JetVel) - FMath::Square(ZTrend);
			float AdjustedForwardStrength = ForwardStrengthSquared > KINDA_SMALL_NUMBER ? FMath::Sqrt(ForwardStrengthSquared) : 0;
			float AdjustedUpStrength = FMath::Min(ZTrend, JetVel) + JetStrength*DeltaSeconds*JetUpRatio;
			Accel += AdjustedForwardStrength * Facing + AdjustedUpStrength * FVector::UpVector;
			ForwardRatio = AdjustedForwardStrength / (JetStrength*DeltaSeconds);
			UpRatio = AdjustedUpStrength / (JetStrength*DeltaSeconds);
		}
	}while (false);
	if (PawnOwner->Role > ROLE_SimulatedProxy)
	{
		if (Acceleration.Size() > KINDA_SMALL_NUMBER)
		{
			FRotator PawnRotation = PawnOwner->GetActorRotation();
			PawnRotation = FMath::RInterpTo(PawnRotation, Acceleration.ToOrientationRotator(), DeltaSeconds, TurnRate);
			PawnOwner->SetActorRotation(PawnRotation);
		}
		Facing = PawnOwner->GetActorForwardVector();
	}
	Acceleration += Accel;
	Velocity += Accel;
}

void UTABCharacterMovementComponent::HandleImpact(const FHitResult& Hit, float TimeSlice/* =0.f */, const FVector& MoveDelta /* = FVector::ZeroVector */)
{
	Super::HandleImpact(Hit, TimeSlice, MoveDelta);
	/*
	const FVector ForceAccel = Acceleration + (IsFalling() ? FVector(0.f, 0.f, GetGravityZ()) : FVector::ZeroVector);
	const FVector_NetQuantizeNormal HitNormal = Hit.Normal;
	if (auto OtherCharacter = Cast<ATABPlayerCharacter>(Hit.GetActor()))
	{
		auto OtherMovement = Cast<UTABCharacterMovementComponent>(OtherCharacter->GetCharacterMovement());
		const FVector OtherForceAccel = OtherMovement->Acceleration + (OtherMovement->IsFalling() ? FVector(0.f, 0.f, OtherMovement->GetGravityZ()) : FVector::ZeroVector);
		const FVector RelativeForce = (Velocity + ForceAccel*TimeSlice - OtherMovement->Velocity - OtherForceAccel*TimeSlice).ProjectOnTo(HitNormal);
		const FVector RelativeAccelForce = (ForceAccel*TimeSlice - OtherForceAccel*TimeSlice).ProjectOnTo(HitNormal);
		this->AddImpulse(-RelativeAccelForce, true);
		OtherMovement->AddImpulse(RelativeForce, true);
	}
	else
	{
		AddImpulse(-Velocity.ProjectOnTo(HitNormal));
	}*/
}


float UTABCharacterMovementComponent::GetMaxSpeed() const
{
	float Result = Super::GetMaxSpeed();
	if (bIsJetting)
	{
		if (!IsMovingOnGround()) Result *= 0.5f * JetSpeedMultiplier + 0.5f;
		else Result *= JetSpeedMultiplier;
	}
	return Result;
}

void FSavedMove_TABCharacter::Clear()
{
	Super::Clear();
	bSavedIsJetting = false;
	SavedFacing = FVector::ZeroVector;
	SavedFuelLeft = 0.f;
}

uint8 FSavedMove_TABCharacter::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();
	if (bSavedIsJetting)
	{
		Result |= FLAG_Custom_0;
	}
	return Result;
}

bool FSavedMove_TABCharacter::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* InPawn, float MaxDelta) const
{
	auto NewSavedMove = reinterpret_cast<const FSavedMove_TABCharacter*>(&NewMove);
	if (bSavedIsJetting != NewSavedMove->bSavedIsJetting)
		return false;
	if (!SavedFacing.Equals(NewSavedMove->SavedFacing, 0.001f))
		return false;
	if (!FMath::IsNearlyEqual(SavedFuelLeft, NewSavedMove->SavedFuelLeft, 0.001f))
		return false;
	return Super::CanCombineWith(NewMove, InPawn, MaxDelta);
}

void FSavedMove_TABCharacter::SetMoveFor(ACharacter* C, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData)
{
	Super::SetMoveFor(C, InDeltaTime, NewAccel, ClientData);
	UTABCharacterMovementComponent* CharMov = Cast<UTABCharacterMovementComponent>(C->GetCharacterMovement());
	if (CharMov)
	{
		bSavedIsJetting = CharMov->bIsJetting;
		SavedFacing = CharMov->Facing;
		SavedFuelLeft = CharMov->FuelLeft;
	}
}

void FSavedMove_TABCharacter::PrepMoveFor(ACharacter* C)
{
	Super::PrepMoveFor(C);
	UTABCharacterMovementComponent* CharMov = Cast<UTABCharacterMovementComponent>(C->GetCharacterMovement());
	if (CharMov)
	{
		CharMov->Facing = SavedFacing;
		CharMov->FuelLeft = SavedFuelLeft;
	}
}

FNetworkPredictionData_Client_TABCharacter::FNetworkPredictionData_Client_TABCharacter(const UCharacterMovementComponent& ClientMovement)
	: Super(ClientMovement)
{

}

FSavedMovePtr FNetworkPredictionData_Client_TABCharacter::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_TABCharacter());
}
