// Fill out your copyright notice in the Description page of Project Settings.

#include "TABOnlineSessionClient.h"
#include "TABGameInstance.h"
#include "TAB.h"



UTABOnlineSessionClient::UTABOnlineSessionClient()
{
}

void UTABOnlineSessionClient::OnSessionUserInviteAccepted(
	const bool							bWasSuccess,
	const int32							ControllerId,
	TSharedPtr< const FUniqueNetId > 	UserId,
	const FOnlineSessionSearchResult &	InviteResult
)
{
	UE_LOG(LogOnline, Verbose, TEXT("HandleSessionUserInviteAccepted: bSuccess: %d, ControllerId: %d, User: %s"), bWasSuccess, ControllerId, UserId.IsValid() ? *UserId->ToString() : TEXT("NULL"));

	if (!bWasSuccess)
	{
		return;
	}

	if (!InviteResult.IsValid())
	{
		UE_LOG(LogOnline, Warning, TEXT("Invite accept returned no search result."));
		return;
	}

	if (!UserId.IsValid())
	{
		UE_LOG(LogOnline, Warning, TEXT("Invite accept returned no user."));
		return;
	}

	UTABGameInstance* TABGameInstance = Cast<UTABGameInstance>(GetGameInstance());

	if (TABGameInstance)
	{
		FTABPendingInvite PendingInvite;

		// Set the pending invite, and then go to the initial screen, which is where we will process it
		PendingInvite.ControllerId = ControllerId;
		PendingInvite.UserId = UserId;
		PendingInvite.InviteResult = InviteResult;
		PendingInvite.bPrivilegesCheckedAndAllowed = false;

		//TABGameInstance->SetPendingInvite(PendingInvite);
		//TABGameInstance->GotoState(TABGameInstanceState::PendingInvite);
	}
}

void UTABOnlineSessionClient::OnPlayTogetherEventReceived(int32 UserIndex, TArray<TSharedPtr<const FUniqueNetId>> UserIdList)
{
	if (UTABGameInstance* const TABGameInstance = Cast<UTABGameInstance>(GetGameInstance()))
	{
		//TABGameInstance->OnPlayTogetherEventReceived(UserIndex, UserIdList);
	}
}