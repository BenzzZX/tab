// Fill out your copyright notice in the Description page of Project Settings.

#include "TABPackage.h"
#include "TABInteractive.h"
#include "TABPlayerCharacter.h"
#include "TABItem.h"
#include "TAB.h"


ATABPackage::ATABPackage()
{
	PrimaryActorTick.bCanEverTick = false;
	TimeToVanish = 10.f;
	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	InteractiveComponent = CreateDefaultSubobject<UTABInteractiveComponent>("InteractiveComponent");
	InteractiveComponent->SetupAttachment(RootComponent);
	InteractiveComponent->OnInteractWith.AddDynamic(this, &ATABPackage::InteractWith);
	InteractiveComponent->OnReceiveFocus.AddDynamic(this, &ATABPackage::OnReceiveFocus);
	SetReplicates(true);
}

void ATABPackage::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
		//ItemIn->SetActorLocation(GetActorLocation());
		//InteractiveComponent->SetInstruction(FText::Format(NSLOCTEXT("TAB", "PackInstructionFormatStr", "����J����{0}"), ItemIn->Name));
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ATABPackage::Vanish, TimeToVanish);
	}
}

void ATABPackage::OnReceiveFocus(UTABInteractiveComponent* Component, ATABPlayerCharacter* PlayerCharacter)
{
	Component->bAcceptFocus = ItemIn->CanPickBy(PlayerCharacter);
}

void ATABPackage::InteractWith(UTABInteractiveComponent* Component, ATABPlayerCharacter* PlayerCharacter)
{
	PlayerCharacter->TakeItem(ItemIn);
	ItemIn = nullptr;
	Component->bAcceptFocus = false;
	Destroy();
}

void ATABPackage::Vanish()
{
	Destroy();
	ItemIn->Destroy();
}


void ATABPackage::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATABPackage, ItemIn);
}