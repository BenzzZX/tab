// Fill out your copyright notice in the Description page of Project Settings.

#include "TABPlayerCharacter.h"
#include "TAB.h"
#include "TABFood.h"
#include "TABInteractive.h"
#include "TABPlayerController.h"
#include "TABCharacterMovementComponent.h"
#include "TABPlayerState.h"
#include "TABPackage.h"

// Sets default values
ATABPlayerCharacter::ATABPlayerCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer.SetDefaultSubobjectClass<UTABCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Thruster_Forward = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, "Thruster_ForwardParticle");
	Thruster_Up = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, "Thruster_UpParticle");
	Thruster_Forward->SetupAttachment(GetMesh());
	Thruster_Up->SetupAttachment(GetMesh());
	bWantsToJet = false;
	bIsJetting = false;
	CharMov = Cast<UTABCharacterMovementComponent>(GetCharacterMovement());

	InteractiveComponent = CreateDefaultSubobject<UTABInteractiveComponent>("InteractiveComponent");
	InteractiveComponent->SetupAttachment(RootComponent);
	
}

// Called when the game starts or when spawned
void ATABPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	InteractiveComponent->OnInteractWith.AddDynamic(this, &ATABPlayerCharacter::InteractWith);
	InteractiveComponent->OnReceiveFocus.AddDynamic(this, &ATABPlayerCharacter::ReceiveFocus);
#define LOCTEXT_NAMESPACE "Interactive"
	InteractiveComponent->SetInstruction(LOCTEXT("Instuction", "Press J"));
#undef LOCTEXT_NAMESPACE
}

// Called every frame
void ATABPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsLocallyControlled() && bWantsToJet != GetIsJetting())
	{
		CharMov->bIsJetting = bWantsToJet;
		if (IsLocallyControlled()) OnChangeStatus.Broadcast();
	}
	if(HasAuthority()||IsLocallyControlled())
		bIsJetting = GetIsJetting();
	if (!bIsJetting)
	{
		Thruster_Forward->SetActive(false);
		Thruster_Up->SetActive(false);
	}
	else
	{
		if (CharMov->IsMovingOnGround())
		{
			Thruster_Up->SetActive(false);
			Thruster_Forward->SetActive(true);
			Thruster_Forward->SetFloatParameter("Strength", 1.f);
		}
		else
		{
			if (CharMov->ForwardRatio > KINDA_SMALL_NUMBER)
			{
				Thruster_Forward->SetActive(true);
				Thruster_Forward->SetFloatParameter("Strength", CharMov->ForwardRatio * 0.7f);
			}
			else
				Thruster_Forward->SetActive(false);
			Thruster_Up->SetActive(true);
			Thruster_Up->SetFloatParameter("Strength", CharMov->UpRatio);
		}
	}
}

// Called to bind functionality to input
void ATABPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveUp", this, &ATABPlayerCharacter::MoveUp);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATABPlayerCharacter::MoveRight);
	PlayerInputComponent->BindAction("Jet", IE_Pressed, this, &ATABPlayerCharacter::JetAction);
	PlayerInputComponent->BindAction("Interact", EInputEvent::IE_Pressed, this, &ATABPlayerCharacter::InteractAction);
	PlayerInputComponent->BindAction("Jet", EInputEvent::IE_Released, this, &ATABPlayerCharacter::StopJetting);
	PlayerInputComponent->BindAction("Use", EInputEvent::IE_Pressed, this, &ATABPlayerCharacter::UseAction);
}

bool ATABPlayerCharacter::GetIsJetting()
{
	return Cast<UTABCharacterMovementComponent>(GetCharacterMovement())->bIsJetting;
}

void ATABPlayerCharacter::OnRep_Statu()
{
	OnChangeStatus.Broadcast();
}

void ATABPlayerCharacter::TakeItem(class ATABItem* Item)
{
	if (ATABFood* Food = Cast<ATABFood>(Item))
	{
		FoodCarrying = Food;
		OnTakeFood();
	}
	else
	{
		ItemCarrying.Add(Item);
		OnTakeItem(ItemCarrying.Num() - 1);
	}
	OnChangeStatus.Broadcast();
}

ATABFood* ATABPlayerCharacter::GiveFood()
{
	OnGiveFood();
	ATABFood* Food = FoodCarrying;
	FoodCarrying = nullptr;
	OnChangeStatus.Broadcast();
	return Food;
}

void ATABPlayerCharacter::MoveUp(float delta)
{
	static FVector Axis = FVector{ 1,0,0 };
	AddMovementInput(Axis, delta);
}

void ATABPlayerCharacter::MoveRight(float delta)
{
	static FVector Axis = FVector{ 0,1,0 };
	AddMovementInput(Axis, delta);
}

void ATABPlayerCharacter::NotifyEnterInteractive(UTABInteractiveComponent* InteractiveComponet)
{
	if (!HasAuthority()) return;
	PendingInteractives.Add(InteractiveComponet);
	UpdateFocusing();
}

void ATABPlayerCharacter::NotifyLeaveInteractive(UTABInteractiveComponent* InteractiveComponet)
{
	if (!HasAuthority()) return;
	PendingInteractives.RemoveSingle(InteractiveComponet);
	UpdateFocusing();
}



void ATABPlayerCharacter::UpdateFocusing()
{
	if (!HasAuthority()) return;
	UTABInteractiveComponent* target = nullptr;
	for (int i = PendingInteractives.Num() - 1; i >= 0; i--)
	{
		target = PendingInteractives[i];
		if (target == FocusingInteractive && target->bAcceptFocus) return; //没有改变，直接返回
		target->ReceiveFocus(this);
		if (target->bAcceptFocus)
		{   //找到了
			if (IsValid(FocusingInteractive)) FocusingInteractive->LoseFocus(this);
			FocusingInteractive = target;
			OnChangeStatus.Broadcast();
			return;
		}
	}
	//没有可交互的
	if (!IsValid(FocusingInteractive)) return;
	FocusingInteractive->LoseFocus(this);
	FocusingInteractive = nullptr;
	OnChangeStatus.Broadcast();
}



void ATABPlayerCharacter::JetAction()
{
	
	if (CharMov->FuelLeft < KINDA_SMALL_NUMBER) return;
	bWantsToJet = true;
	CharMov->bIsJetting = bWantsToJet;
	if(IsLocallyControlled()) OnChangeStatus.Broadcast();

}

void ATABPlayerCharacter::StopJetting()
{
	bWantsToJet = false;
	CharMov->bIsJetting = bWantsToJet;
	if (IsLocallyControlled()) OnChangeStatus.Broadcast();
}

void ATABPlayerCharacter::UseAction()
{
	if(IsValid(FoodCarrying)) ServerDrop();
	else ServerUse();
}

bool ATABPlayerCharacter::ServerInteract_Validate()
{
	return true;
}

void ATABPlayerCharacter::ServerInteract_Implementation()
{
	if (!IsValid(FocusingInteractive)) return;
	FocusingInteractive->InteractWith(this);
	if (IsValid(FocusingInteractive) && FocusingInteractive->bAcceptFocus) return;
	UpdateFocusing();
}

void ATABPlayerCharacter::InteractAction()
{
	if (IsValid(FocusingInteractive)) FocusingInteractive->InteractWith(this);
	if (HasAuthority())
	{
		if (IsValid(FocusingInteractive) && FocusingInteractive->bAcceptFocus) return;
		UpdateFocusing(); return;
	}
	if (Role == ROLE_AutonomousProxy) ServerInteract();
}

bool ATABPlayerCharacter::ServerDrop_Validate()
{
	return true;
}

void ATABPlayerCharacter::ServerDrop_Implementation()
{
	
	ATABPackage* Package = Cast<ATABPackage>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, FoodPackageClass, GetActorTransform()));
	if (IsValid(Package))
	{
		Package->ItemIn = GiveFood();
		UGameplayStatics::FinishSpawningActor(Package, GetActorTransform());
	}
	UpdateFocusing();
}

bool ATABPlayerCharacter::ServerUse_Validate()
{
	return true;
}


void ATABPlayerCharacter::ServerUse_Implementation()
{
	if (ItemCarrying.Num() <= 0) return;
	OnUseItem(ItemCarrying.Num() - 1);
	ItemCarrying.Pop()->OnUse(this);
	UpdateFocusing();
}


void ATABPlayerCharacter::ReceiveFocus(UTABInteractiveComponent* Component, ATABPlayerCharacter* PlayerCharacter)
{
	if (auto PlayerState = Cast<ATABPlayerState>(PlayerCharacter->PlayerState))
	{
		
		UE_LOG(LogTABGame, Log, TEXT("%s Focus %s"), *PlayerCharacter->GetName(), *GetName());
		InteractiveComponent->bAcceptFocus = IsValid(FoodCarrying) && FoodCarrying->TeamID == PlayerState->GetTeamID() && !IsValid(PlayerCharacter->FoodCarrying);
	}
	else InteractiveComponent->bAcceptFocus = false;
}

void ATABPlayerCharacter::InteractWith(UTABInteractiveComponent* Component, ATABPlayerCharacter* PlayerPawn)
{
	PlayerPawn->TakeItem(GiveFood());
	bWantsToJet = false;
	if (auto PC = Cast<APlayerController>(GetController()))
		DisableInput(PC);
	GetWorld()->GetTimerManager().SetTimer(Stunned, this, &ATABPlayerCharacter::Recovery, 1.0, false);
	InteractiveComponent->bAcceptFocus = false;
}

void ATABPlayerCharacter::Recovery()
{
	if (auto PC = Cast<APlayerController>(GetController()))
		EnableInput(PC);
}


void ATABPlayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATABPlayerCharacter, ItemCarrying);
	DOREPLIFETIME(ATABPlayerCharacter, FoodCarrying);
	DOREPLIFETIME(ATABPlayerCharacter, FocusingInteractive);
	DOREPLIFETIME(ATABPlayerCharacter, bIsJetting);
}