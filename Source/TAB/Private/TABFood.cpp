// Fill out your copyright notice in the Description page of Project Settings.

#include "TABFood.h"
#include "TAB.h"
#include "TABPlayerCharacter.h"
#include "TimerManager.h"


// Sets default values
ATABFood::ATABFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);
	bIsOverdue = false;
#define LOCTEXT_NAMESPACE "TABFood" 
	switch (Info.Type)
	{
	case ETABFoodType::Hamburger:
		Name = LOCTEXT("FoodName", "Hamburger"); break;
	case ETABFoodType::Chip:
		Name = LOCTEXT("FoodName", "Chip"); break;
	case ETABFoodType::Coffee:
		Name = LOCTEXT("FoodName", "Coffee"); break;
	default:
		Name = FText::GetEmpty(); break;
	}
#undef LOCTEXT_NAMESPACE
}

// Called when the game starts or when spawned
void ATABFood::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimeToBeOverdueTimerHandle, this, &ATABFood::BecomeOutdue, TimeToBeOverdue);
	
}

// Called every frame
void ATABFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATABFood::OnUse_Implementation(class ATABPlayerCharacter* PlayerCharacter)
{
	PlayerCharacter->ServerDrop();
}

bool ATABFood::CanPickBy_Implementation(class ATABPlayerCharacter* PlayerCharacter)
{
	return !IsValid(PlayerCharacter->FoodCarrying);
}

void ATABFood::BecomeOutdue()
{
	GetWorldTimerManager().ClearTimer(TimeToBeOverdueTimerHandle);
	bIsOverdue = true;
}

void ATABFood::OnUse(class ATABPlayerCharacter* Character)
{
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
}

void ATABFood::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATABFood, TeamID);
	DOREPLIFETIME(ATABFood, bIsOverdue);
	DOREPLIFETIME(ATABFood, Info);
}