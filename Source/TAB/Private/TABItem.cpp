// Fill out your copyright notice in the Description page of Project Settings.

#include "TABItem.h"
#include "TAB.h"
#include "TABPlayerCharacter.h"


// Sets default values
ATABItem::ATABItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Name = FText::GetEmpty();
	SetReplicates(true);
}

// Called when the game starts or when spawned
void ATABItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATABItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATABItem::OnUse_Implementation(class ATABPlayerCharacter* PlayerCharacter)
{

}

bool ATABItem::CanPickBy_Implementation(class ATABPlayerCharacter* PlayerCharacter)
{
	return PlayerCharacter->ItemCarrying.Num() < 3;
}
